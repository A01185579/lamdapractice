﻿using LamdaPractice.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace LamdaPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new DatabaseContext())
            {

                //DatabaseContext db = new DatabaseContext();
                
                //var em = db.Employees.ToList().ForEach(c => Console.WriteLine());

                //Primer Punto Resuelto: Listar todos los empleados cuyo departamento tenga una sede en Chihuahua
                ctx.Employees.ToList().FindAll(emp => emp.City.Name == "Chihuahua").ForEach(a => Console.WriteLine(a.FirstName + " " + a.LastName + " " + a.City.Name));
               
                //Segundo punto Resuelto: Listar todos los departamentos y el numero de empleados que pertenezcan a cada departamento.
                //Listar para cada departamento, Nombre de departamento + Conteo de empleados donde el DepartmentId == Id.
                ctx.Departments.ToList().ForEach(dep => Console.WriteLine( dep.Name + " " + ctx.Employees.Where(emp => emp.DepartmentId == dep.Id).Count()));

                //Tercer punto :Listar todos los empleados remotos. Estos son los empleados cuya ciudad no se encuentre entre las sedes de su departamento.

                //Cuato punto Resuelto: Listar todos los empleados cuyo aniversario de contratación sea el próximo mes.
                ctx.Employees.ToList().FindAll(emp => emp.HireDate.Month == 5).ForEach(a => Console.WriteLine(a.FirstName + " " + a.LastName + " " + a.HireDate));

                //Quinto punto Resuelto: Listar los 12 meses del año y el numero de empleados contratados por cada mes.
                ctx.Employees.GroupBy(m => m.HireDate.Month).Select(g => new { ID = g.Key, Count = g.Count() }).AsEnumerable().OrderBy(e => e.ID).ToList().ForEach(emp => Console.WriteLine(emp.ID + " " + emp.Count));
                
            }

            Console.Read();
        }
    }
}
